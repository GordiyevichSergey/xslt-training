package com.soapserver.core.filters.impl;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;
import java.util.Map;

import com.soapserver.core.filters.PostFilter;
import com.soapserver.entities.HotelsRequest;
import com.soapserver.entities.HotelsResponse;
import com.soapserver.entities.HotelType;
import com.soapserver.entities.HotelImagesType;

public class GroupImagesFilter implements PostFilter<HotelsRequest, HotelsResponse> {

	@Override
	public boolean isApplicable(final HotelsRequest request, final HotelsResponse response) {
		if (response.getHotel().isEmpty()) {
			return false;
		}
		return true;
	}

	@Override
	public void postProcess(final HotelsRequest request, final HotelsResponse response) {
		final List<HotelType> hotelsDeleteList = new ArrayList<>();
		final Map<String, List<HotelImagesType.HotelImage>> hotelImagesMap = new HashMap<>();
		for (final HotelType hotel : response.getHotel()) {
			final String hotelName = hotel.getHotel().getName();
			if (hotelImagesMap.containsKey(hotelName)) {
				hotelsDeleteList.add(hotel);
			}
			
			if (hotel.getHotelImages() == null) {
				continue;
			}
			
			List<HotelImagesType.HotelImage> hotelImages = hotelImagesMap.get(hotelName);
			if (hotelImages == null) {
				hotelImages = new ArrayList<>();
				hotelImagesMap.put(hotelName, hotelImages);
			}
			hotelImages.addAll(hotel.getHotelImages().getHotelImage());
		}
		
		response.getHotel().removeAll(hotelsDeleteList);
		
		final Iterator<HotelType> hotelsIterator = response.getHotel().iterator();
		while (hotelsIterator.hasNext()) {
			final HotelType hotel = hotelsIterator.next();
			final HotelImagesType hotelImages = hotel.getHotelImages();
			if (hotelImages == null) {
				continue;
			}
			
			hotelImages.getHotelImage().clear();
			
			final List<HotelImagesType.HotelImage> hotelImagesList = hotelImagesMap.get(hotel.getHotel().getName());
			if (hotelImagesList != null) {
				hotelImages.getHotelImage().addAll(hotelImagesList);
			}
		}
	}
	
}