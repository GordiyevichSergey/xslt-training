<?xml version="1.0"?>
<xsl:stylesheet version="1.0" xmlns:xsl="http://www.w3.org/1999/XSL/Transform"
	xmlns:ent="http://soapserver.com/entities"
	xmlns:requtil="http://xml.apache.org/xalan/java/com.soapserver.core.helpers.RequestUtil"
	exclude-result-prefixes="ent requtil">
	
	<xsl:output method="text"/>
	
	<xsl:template match="/">
		<xsl:variable name="countryCode" select="ent:HotelsRequest/ent:CountryCode"/>
		<xsl:variable name="cityName" select="ent:HotelsRequest/ent:CityName"/>
		<xsl:variable name="language" select="ent:HotelsRequest/ent:Language"/>
		<xsl:variable name="hotelName" select="ent:HotelsRequest/ent:HotelName"/>
		<xsl:variable name="hotelCategory" select="ent:HotelsRequest/ent:HotelCategory"/>
		
		<xsl:text>
			select cntName.NAME as countryName, cnt.CODE as countryCode, cityName.NAME as cityName, city.CODE as cityCode, hotName.NAME as hotelName, hot.CODE as hotelCode, hot.PHONE as hotelPhone, hot.FAX as hotelFax, hot.EMAIL as hotelEmail, hot.URL as hotelURL, hot.CHECK_IN as hotelCheckIn, hot.CHECK_OUT as hotelCheckOut, hot.LATITUDE as hotelLatitude, hot.LONGITUDE as hotelLongitude, hotImgs.URL as hotelImageURL, hotImgType.TYPE_NAME as hotelImageTypeName
			from gpt_hotel as hot
			join gpt_location as cnt on hot.COUNTRY_ID = cnt.ID
			join gpt_location_name as cntName on cnt.ID = cntName.LOCATION_ID
			join gpt_location as city on hot.CITY_ID = city.ID
			join gpt_location_name as cityName on city.ID = cityName.LOCATION_ID
			join gpt_hotel_name as hotName on hotName.HOTEL_ID = hot.ID
			join gpt_hotel_image_type as hotImgType
			join gpt_hotel_images as hotImgs on hot.ID = hotImgs.HOTEL_ID and hotImgType.ID = hotImgs.TYPE_ID
			join gpt_language as cntNameLng on cntName.LANG_ID = cntNameLng.ID
			join gpt_language as cityNameLng on cityName.LANG_ID = cityNameLng.ID
			join gpt_language as hotNameLng on hotName.LANG_ID = hotNameLng.ID			
		</xsl:text>
		<xsl:text>where cnt.CODE="</xsl:text>
		<xsl:value-of select="$countryCode"/>
		<xsl:text>"</xsl:text>
		
		<xsl:text>and cntNameLng.CODE="</xsl:text>
		<xsl:value-of select="$language"/>
		<xsl:text>" and cityNameLng.CODE="</xsl:text>
		<xsl:value-of select="$language"/>
		<xsl:text>" and hotNameLng.CODE="</xsl:text>
		<xsl:value-of select="$language"/>
		<xsl:text>"</xsl:text>
		
		<xsl:if test="$cityName != ''">
			<xsl:text> and cityName.NAME="</xsl:text>
			<xsl:value-of select="$cityName"/>
			<xsl:text>"</xsl:text>
		</xsl:if>
		
		<xsl:if test="$hotelName != ''">
			<xsl:text> and hotName.NAME like '%</xsl:text>
			<xsl:value-of select="$hotelName"/>
			<xsl:text>%'</xsl:text>
		</xsl:if>
		
		<xsl:if test="$hotelCategory !=''">
			<xsl:text> and hot.CATEGORY_ID="</xsl:text>
			<xsl:value-of select="$hotelCategory"/>
			<xsl:text>"</xsl:text>
		</xsl:if>
		
	</xsl:template>
	
</xsl:stylesheet>