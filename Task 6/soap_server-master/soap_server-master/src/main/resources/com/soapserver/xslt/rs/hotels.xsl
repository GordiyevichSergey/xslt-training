<?xml version="1.0"?>
<xsl:stylesheet version="1.0" xmlns:xsl="http://www.w3.org/1999/XSL/Transform"
	xmlns:ent="http://soapserver.com/entities"
	xmlns:sutil="http://xml.apache.org/xalan/java/com.soapserver.core.helpers.StringUtil"
	xmlns:queryutil="http://xml.apache.org/xalan/java/com.soapserver.core.helpers.SqlQueriesUtil"
	exclude-result-prefixes="ent sutil queryutil">
	
	<xsl:output method="xml"/>
	
	<xsl:template match="/">
		<ent:HotelsResponse>
			<xsl:for-each select="Result/Entry">
				<xsl:variable name="country_name" select="countryName"/>
				<xsl:variable name="country_code" select="countryCode"/>
				<xsl:variable name="city_name" select="cityName"/>
				<xsl:variable name="city_code" select="cityCode"/>
				<xsl:variable name="hotel_name" select="hotelName"/>
				<xsl:variable name="hotel_code" select="hotelCode"/>
				<xsl:variable name="hotel_latitude" select="hotelLatitude"/>
				<xsl:variable name="hotel_longitude" select="hotelLongitude"/>
				<xsl:variable name="hotel_image_url" select="hotelImageURL"/>
				<xsl:variable name="hotel_image_type_name" select="hotelImageTypeName"/>
							
				<ent:Hotel>
					<ent:Country Name="{$country_name}" Code="{$country_code}"/>
					<ent:City Name="{$city_name}" Code="{$city_code}"/>
					<ent:Hotel_ Name="{$hotel_name}" Code="{$hotel_code}"/>
					<ent:Phone>
						<xsl:value-of select="hotelPhone"/>
					</ent:Phone>
					<ent:Fax>
						<xsl:value-of select="hotelFax"/>
					</ent:Fax>
					<ent:Email>
						<xsl:value-of select="hotelEmail"/>
					</ent:Email>
					<ent:URL>
						<xsl:value-of select="hotelURL"/>
					</ent:URL>
					<ent:Check-in>
						<xsl:value-of select="hotelCheckIn"/>
					</ent:Check-in>
					<ent:Check-out>
						<xsl:value-of select="hotelCheckOut"/>
					</ent:Check-out>
					<ent:Coordinates Latitude="{$hotel_latitude}" Longitude="{$hotel_longitude}"/>
					<ent:HotelImages>
						<ent:HotelImage URL="{$hotel_image_url}" TypeName="{$hotel_image_type_name}"/>
					</ent:HotelImages>
				</ent:Hotel>
			</xsl:for-each>
		</ent:HotelsResponse>
	</xsl:template>
	
</xsl:stylesheet>