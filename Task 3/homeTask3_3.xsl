<?xml version="1.0" encoding="UTF-8"?>
<xsl:stylesheet version="1.0" 
    xmlns:xsl="http://www.w3.org/1999/XSL/Transform">
    <xsl:output indent="yes"/>
    <xsl:key name="k" match="item" use="substring(@name,1,1)"/>
    
    <xsl:template match="list">
        <list>
            <xsl:apply-templates select="item[generate-id(.)=generate-id(key('k',substring(@name,1,1)))]">
                <xsl:sort select="@name"/>
            </xsl:apply-templates>
        </list>
    </xsl:template>
    
    <xsl:template match="item">
        <capital value="{substring(@name,1,1)}">
            <xsl:for-each select="key('k',substring(@name,1,1))">
                <xsl:sort select="@name"/>
                <xsl:copy-of select="."/>
            </xsl:for-each>
        </capital>
    </xsl:template>
</xsl:stylesheet>