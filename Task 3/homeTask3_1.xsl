<?xml version="1.0" encoding="UTF-8"?>
<xsl:stylesheet version="1.0"
    xmlns:xsl="http://www.w3.org/1999/XSL/Transform">
    <xsl:output indent="yes"/>
    
    <xsl:template match="/">
        <Guests>
            <xsl:call-template name="stringToElements">
                <xsl:with-param name="string" select="."/>
            </xsl:call-template>
        </Guests>
    </xsl:template>
    
    <xsl:template name="stringToElements">
        <xsl:param name="string"/>
        
        <xsl:if test="string-length($string) &gt; 0">
            <xsl:variable name="guestString" select="substring-before($string,'#')"/>
            
            <xsl:variable name="age">
                <xsl:call-template name="getSubstringFromGuestString">
                    <xsl:with-param name="string" select="$guestString"/>
                    <xsl:with-param name="loopMax" select="0"/>
                </xsl:call-template>
            </xsl:variable>
            <xsl:variable name="nationality">
                <xsl:call-template name="getSubstringFromGuestString">
                    <xsl:with-param name="string" select="$guestString"/>
                    <xsl:with-param name="loopMax" select="1"/>
                </xsl:call-template>
            </xsl:variable>
            <xsl:variable name="gender">
                <xsl:call-template name="getSubstringFromGuestString">
                    <xsl:with-param name="string" select="$guestString"/>
                    <xsl:with-param name="loopMax" select="2"/>
                </xsl:call-template>
            </xsl:variable>
            <xsl:variable name="name">
                <xsl:call-template name="getSubstringFromGuestString">
                    <xsl:with-param name="string" select="$guestString"/>
                    <xsl:with-param name="loopMax" select="3"/>
                </xsl:call-template>
            </xsl:variable>
            <xsl:variable name="type">
                <xsl:call-template name="getSubstringFromGuestString">
                    <xsl:with-param name="string" select="$guestString"/>
                    <xsl:with-param name="loopMax" select="4"/>
                </xsl:call-template>
            </xsl:variable>
            <xsl:variable name="address">
                <xsl:call-template name="getSubstringFromGuestString">
                    <xsl:with-param name="string" select="$guestString"/>
                    <xsl:with-param name="loopMax" select="5"/>
                </xsl:call-template>
            </xsl:variable>
                
            <Guest Age='{$age}' Nationality='{$nationality}' Gender='{$gender}' Name='{$name}'>
                <Type><xsl:value-of select="$type"/></Type>
                <Profile>
                    <Address><xsl:value-of select="$address"/></Address>
                </Profile>
            </Guest>
            
            <xsl:call-template name="stringToElements">
                <xsl:with-param name="string" select="substring-after($string,'#')"/>
            </xsl:call-template>
        </xsl:if>
    </xsl:template>
    
    <xsl:template name="getSubstringFromGuestString">
        <xsl:param name="string"/>
        <xsl:param name="loopMax"/>
        <xsl:param name="loopCur" select="0"/>
        
        <xsl:choose>
            <xsl:when test="$loopCur = $loopMax">
                <xsl:value-of select="substring-before($string,'|')"/>
            </xsl:when>
            <xsl:otherwise>
                <xsl:call-template name="getSubstringFromGuestString">
                    <xsl:with-param name="string" select="substring-after($string,'|')"/>
                    <xsl:with-param name="loopMax" select="$loopMax"/>
                    <xsl:with-param name="loopCur" select="$loopCur + 1"/>
                </xsl:call-template>
            </xsl:otherwise>
        </xsl:choose>
    </xsl:template>
    
</xsl:stylesheet>