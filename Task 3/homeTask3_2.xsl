<?xml version="1.0" encoding="UTF-8"?>
<xsl:stylesheet version="1.0" 
    xmlns:xsl="http://www.w3.org/1999/XSL/Transform">
    <xsl:output indent="yes"/>
    
    <xsl:template match="@* | node()">
        <xsl:copy>
            <xsl:apply-templates select="@* | node()"/>
        </xsl:copy>
    </xsl:template>
    
    <xsl:template match="RoomStays">
        <Hotels>
            <xsl:apply-templates select="@* | node()"/>
        </Hotels>
    </xsl:template>
    
    <xsl:template match="RoomStay">
        <Hotel>
            <xsl:apply-templates select="@* | node()"/>
        </Hotel>
    </xsl:template>
    
    <xsl:template match="RoomRates">
        <Offers>
            <xsl:apply-templates select="@* | node()"/>
        </Offers>
    </xsl:template>
    
    <xsl:template match="RoomRate">
        <Offer>
            <xsl:apply-templates select="@* | node()"/>
        </Offer>
    </xsl:template>
    
    <xsl:template match="@AmountAfterTax">
        <xsl:attribute name="{name()}">
            <xsl:value-of select="round(.)"/>
        </xsl:attribute>
    </xsl:template>
</xsl:stylesheet>