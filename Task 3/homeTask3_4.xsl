<?xml version="1.0" encoding="UTF-8"?>
<xsl:stylesheet version="1.0" 
    xmlns:xsl="http://www.w3.org/1999/XSL/Transform">
    <xsl:output indent="yes"/>
    <xsl:key name="name" match="* | @*" use="name()"/>
    
   <xsl:template match="/">
        <root>
            <xsl:apply-templates select="* | @*"/>
        </root>
    </xsl:template>
   
    <xsl:template match="* | @*">
        <xsl:if test="generate-id() = generate-id(key('name',name()))">
            Node '<xsl:value-of select="name()"/>' found <xsl:value-of select="count(key('name',name()))"/> times
        </xsl:if>
        <xsl:apply-templates select="* | @*"/>
    </xsl:template>
</xsl:stylesheet>