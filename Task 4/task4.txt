1.
select count(*)
from gpt_hotel
where CITY_ID = 4;

2.
select NAME
from gpt_hotel_name 
where NAME like '%Hilton%' and LANG_ID = 1 and HOTEL_ID in (select id from gpt_hotel where active = 1);

3. 
select gpt_location_name.NAME, gpt_hotel_name.NAME, gpt_hotel.PHONE, gpt_hotel.EMAIL, gpt_hotel.LATITUDE, gpt_hotel.LONGITUDE
from gpt_hotel 
join gpt_location_name on gpt_location_name.LOCATION_ID = gpt_hotel.CITY_ID and gpt_location_name.LANG_ID = 1 
join gpt_hotel_name on gpt_hotel_name.HOTEL_ID = gpt_hotel.ID and gpt_hotel_name.LANG_ID = 1 
where gpt_hotel.CITY_ID = 4 and gpt_hotel.ACTIVE = 1;

4.
select gpt_hotel_name.NAME
from gpt_hotel_name 
join gpt_hotel_images on gpt_hotel_name.HOTEL_ID = gpt_hotel_images.HOTEL_ID
where gpt_hotel_name.LANG_ID = 1
group by NAME having count(gpt_hotel_images.HOTEL_ID) > 3;

5.
select concat(gpt_hotel_name.NAME, ', ', gpt_hotel.LATITUDE, ', ', gpt_hotel.LONGITUDE, ', ', gpt_hotel.CATEGORY_ID, ', ', gpt_location_name.NAME)
from gpt_hotel
join gpt_hotel_name on gpt_hotel_name.HOTEL_ID = gpt_hotel.ID and gpt_hotel_name.LANG_ID = 1
join gpt_location_name on gpt_location_name.LOCATION_ID = gpt_hotel.CITY_ID and gpt_location_name.LANG_ID = 1;

6.
select max(ID)
from gpt_hotel_images;

7.
select NAME
from gpt_location_name
join gpt_location on gpt_location.TYPE_ID = 2 and gpt_location_name.LOCATION_ID = gpt_location.ID
where LANG_ID = 4;