-- MySQL dump 10.13  Distrib 5.7.20, for Win64 (x86_64)
--
-- Host: localhost    Database: hotels
-- ------------------------------------------------------
-- Server version	5.7.20-log

/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8 */;
/*!40103 SET @OLD_TIME_ZONE=@@TIME_ZONE */;
/*!40103 SET TIME_ZONE='+00:00' */;
/*!40014 SET @OLD_UNIQUE_CHECKS=@@UNIQUE_CHECKS, UNIQUE_CHECKS=0 */;
/*!40014 SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0 */;
/*!40101 SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_AUTO_VALUE_ON_ZERO' */;
/*!40111 SET @OLD_SQL_NOTES=@@SQL_NOTES, SQL_NOTES=0 */;

--
-- Table structure for table `gpt_hotel`
--

DROP TABLE IF EXISTS `gpt_hotel`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `gpt_hotel` (
  `ID` bigint(20) NOT NULL AUTO_INCREMENT,
  `CODE` varchar(32) NOT NULL,
  `CITY_ID` bigint(20) NOT NULL,
  `COUNTRY_ID` bigint(20) NOT NULL,
  `CATEGORY_ID` enum('Hotel','Hostel','Apartment') NOT NULL,
  `CHAIN_ID` bigint(20) NOT NULL,
  `PHONE` varchar(100) NOT NULL,
  `FAX` varchar(50) NOT NULL,
  `URL` varchar(255) NOT NULL,
  `CHECK_IN` time NOT NULL,
  `CHECK_OUT` time NOT NULL,
  `EMAIL` varchar(255) NOT NULL,
  `ZIP` varchar(50) NOT NULL,
  `LATITUDE` decimal(10,6) NOT NULL,
  `LONGITUDE` decimal(10,6) NOT NULL,
  `ACTIVE` tinyint(1) NOT NULL,
  `DEFAULT_IMAGE` bigint(20) NOT NULL,
  `CREATED` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  PRIMARY KEY (`ID`),
  KEY `CITY_ID` (`CITY_ID`),
  KEY `COUNTRY_ID` (`COUNTRY_ID`),
  CONSTRAINT `gpt_hotel_ibfk_1` FOREIGN KEY (`CITY_ID`) REFERENCES `gpt_location` (`ID`),
  CONSTRAINT `gpt_hotel_ibfk_2` FOREIGN KEY (`COUNTRY_ID`) REFERENCES `gpt_location` (`PARENT`)
) ENGINE=InnoDB AUTO_INCREMENT=7 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `gpt_hotel`
--

LOCK TABLES `gpt_hotel` WRITE;
/*!40000 ALTER TABLE `gpt_hotel` DISABLE KEYS */;
INSERT INTO `gpt_hotel` VALUES (1,'f2333',4,3,'Hotel',1454,'+375291234567','3123','yoyo.com','12:43:11','15:33:14','yoyo@gmail.com','223765',54.210000,12.110000,1,4533,'2017-10-28 17:37:36'),(2,'grtg23',4,3,'Hostel',5532,'+375297654567','3333','yaya.com','12:43:11','15:33:14','yaya@gmail.com','223725',64.210000,11.110000,1,4539,'2017-10-28 17:39:27'),(3,'bgjf12',4,3,'Apartment',5872,'+375297329067','2133','yuyu.com','12:43:11','15:33:14','yuyu@gmail.com','213725',74.210000,21.110000,0,1237,'2017-10-28 17:40:51'),(4,'gjf21',9,8,'Hostel',6764,'+8490238452','9623','proverkanaproverkudz.com','12:43:11','15:33:14','vtorayaproverka@gmail.com','725765',-54.210000,112.110000,1,4533,'2017-10-28 17:47:02'),(5,'ioor90',12,10,'Hotel',1264,'+903483232','7777','tytmoglabitvashareklama.com','12:43:11','15:33:14','avottytnemogla@gmail.com','726565',-54.210000,112.110000,0,1533,'2017-10-28 17:49:28'),(6,'qbor23',7,5,'Hotel',1234,'+345483232','9777','prodamgarag.com','12:43:11','15:33:14','prodamgarag@gmail.com','426565',-54.210000,42.770000,1,2533,'2017-10-28 17:52:27');
/*!40000 ALTER TABLE `gpt_hotel` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `gpt_hotel_image_type`
--

DROP TABLE IF EXISTS `gpt_hotel_image_type`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `gpt_hotel_image_type` (
  `ID` tinyint(3) NOT NULL AUTO_INCREMENT,
  `TYPE_NAME` varchar(128) DEFAULT NULL,
  PRIMARY KEY (`ID`)
) ENGINE=InnoDB AUTO_INCREMENT=4 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `gpt_hotel_image_type`
--

LOCK TABLES `gpt_hotel_image_type` WRITE;
/*!40000 ALTER TABLE `gpt_hotel_image_type` DISABLE KEYS */;
INSERT INTO `gpt_hotel_image_type` VALUES (1,'General'),(2,'Lobby'),(3,'Room');
/*!40000 ALTER TABLE `gpt_hotel_image_type` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `gpt_hotel_images`
--

DROP TABLE IF EXISTS `gpt_hotel_images`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `gpt_hotel_images` (
  `ID` bigint(20) NOT NULL AUTO_INCREMENT,
  `HOTEL_ID` bigint(20) NOT NULL,
  `TYPE_ID` tinyint(3) NOT NULL,
  `URL` varchar(600) NOT NULL,
  `THUMBNAIL_URL` varchar(600) NOT NULL,
  PRIMARY KEY (`ID`),
  KEY `HOTEL_ID` (`HOTEL_ID`),
  KEY `TYPE_ID` (`TYPE_ID`),
  CONSTRAINT `gpt_hotel_images_ibfk_1` FOREIGN KEY (`HOTEL_ID`) REFERENCES `gpt_hotel` (`ID`),
  CONSTRAINT `gpt_hotel_images_ibfk_2` FOREIGN KEY (`TYPE_ID`) REFERENCES `gpt_hotel_image_type` (`ID`)
) ENGINE=InnoDB AUTO_INCREMENT=20 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `gpt_hotel_images`
--

LOCK TABLES `gpt_hotel_images` WRITE;
/*!40000 ALTER TABLE `gpt_hotel_images` DISABLE KEYS */;
INSERT INTO `gpt_hotel_images` VALUES (1,1,1,'1','q'),(2,1,2,'type2(1)','sf'),(3,1,2,'type2(2)','fsgd'),(4,1,3,'type3(1)','dfjgk'),(5,2,1,'type1(1)','erjk'),(6,2,2,'type2(1)','ggrf'),(7,3,1,'type1(1)','erjkje'),(8,3,1,'type1(2)','trjhd'),(9,3,3,'type3(1)','ruthjd'),(10,3,3,'type3(2)','popsdj'),(11,4,1,'type1(1)','tuiuiwe'),(12,5,1,'type1(1)','irutijk'),(13,5,2,'type2(1)','urtijkf'),(14,5,3,'type3(1)','tjkfqws'),(15,6,1,'type1(1)','rtoikjd'),(16,6,1,'type1(2)','tkjdfsd'),(17,6,2,'type2(1)','ptyikjf'),(18,6,3,'type3(1)','rtpkfdl'),(19,6,3,'type3(2)','rkjfkdg');
/*!40000 ALTER TABLE `gpt_hotel_images` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `gpt_hotel_name`
--

DROP TABLE IF EXISTS `gpt_hotel_name`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `gpt_hotel_name` (
  `HOTEL_ID` bigint(20) NOT NULL,
  `LANG_ID` tinyint(3) NOT NULL,
  `NAME` varchar(500) NOT NULL,
  KEY `HOTEL_ID` (`HOTEL_ID`),
  KEY `LANG_ID` (`LANG_ID`),
  CONSTRAINT `gpt_hotel_name_ibfk_1` FOREIGN KEY (`HOTEL_ID`) REFERENCES `gpt_hotel` (`ID`),
  CONSTRAINT `gpt_hotel_name_ibfk_2` FOREIGN KEY (`LANG_ID`) REFERENCES `gpt_language` (`ID`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `gpt_hotel_name`
--

LOCK TABLES `gpt_hotel_name` WRITE;
/*!40000 ALTER TABLE `gpt_hotel_name` DISABLE KEYS */;
INSERT INTO `gpt_hotel_name` VALUES (1,1,'DoubleTree by Hilton'),(2,1,'Hotel Belarus'),(2,2,'Hotel Weissrussland'),(2,4,'Отель Беларусь'),(3,1,'Hotel Yubileiny'),(3,4,'Отель Юбилейный'),(4,1,'Hilton Orange County'),(5,1,'Hilton Munich City'),(5,2,'Hilton M?nchen City'),(6,1,'Nevsky Hotel Grand'),(6,4,'Невский Отель Гранд');
/*!40000 ALTER TABLE `gpt_hotel_name` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `gpt_language`
--

DROP TABLE IF EXISTS `gpt_language`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `gpt_language` (
  `ID` tinyint(3) NOT NULL AUTO_INCREMENT,
  `code` varchar(2) NOT NULL,
  `name` varchar(40) NOT NULL,
  PRIMARY KEY (`ID`)
) ENGINE=InnoDB AUTO_INCREMENT=6 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `gpt_language`
--

LOCK TABLES `gpt_language` WRITE;
/*!40000 ALTER TABLE `gpt_language` DISABLE KEYS */;
INSERT INTO `gpt_language` VALUES (1,'EN','English'),(2,'DE','German'),(3,'FR','French'),(4,'RU','Russian'),(5,'BY','Belorussian');
/*!40000 ALTER TABLE `gpt_language` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `gpt_location`
--

DROP TABLE IF EXISTS `gpt_location`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `gpt_location` (
  `ID` bigint(20) NOT NULL AUTO_INCREMENT,
  `CODE` varchar(32) NOT NULL,
  `TYPE_ID` tinyint(3) NOT NULL,
  `PARENT` bigint(20) DEFAULT NULL,
  `ACTIVE` tinyint(1) NOT NULL,
  `LATITUDE` decimal(10,6) NOT NULL,
  `LONGITUDE` decimal(10,6) NOT NULL,
  `TIME_ZONE` varchar(50) NOT NULL,
  PRIMARY KEY (`ID`),
  KEY `TYPE_ID` (`TYPE_ID`),
  KEY `PARENT` (`PARENT`),
  CONSTRAINT `gpt_location_ibfk_1` FOREIGN KEY (`TYPE_ID`) REFERENCES `gpt_location_type` (`ID`),
  CONSTRAINT `gpt_location_ibfk_2` FOREIGN KEY (`PARENT`) REFERENCES `gpt_location` (`ID`)
) ENGINE=InnoDB AUTO_INCREMENT=13 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `gpt_location`
--

LOCK TABLES `gpt_location` WRITE;
/*!40000 ALTER TABLE `gpt_location` DISABLE KEYS */;
INSERT INTO `gpt_location` VALUES (1,'0000000000',1,NULL,1,0.000000,0.000000,'UTC'),(3,'0by0000000',2,1,1,10.150000,117.020000,'UTC+5'),(4,'0by0000001',3,3,1,11.220000,123.670000,'UTC+5'),(5,'0ru0000000',2,1,1,37.350000,-75.020000,'UTC+6'),(6,'0ru0000001',3,5,1,37.350000,-75.020000,'UTC+6'),(7,'0ru0000002',3,5,1,50.110000,-30.020000,'UTC+7'),(8,'0us0000000',2,1,1,-112.290000,85.500000,'UTC-8'),(9,'0us0000001',3,8,1,-112.290000,85.500000,'UTC-8'),(10,'0de0000000',2,1,1,112.290000,-85.500000,'UTC+4'),(11,'0de0000001',3,10,1,112.290000,-85.500000,'UTC+4'),(12,'0de0000002',3,10,1,11.290000,-85.500000,'UTC+4');
/*!40000 ALTER TABLE `gpt_location` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `gpt_location_name`
--

DROP TABLE IF EXISTS `gpt_location_name`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `gpt_location_name` (
  `LOCATION_ID` bigint(20) NOT NULL,
  `LANG_ID` tinyint(3) NOT NULL,
  `NAME` varchar(255) NOT NULL,
  KEY `LOCATION_ID` (`LOCATION_ID`),
  KEY `LANG_ID` (`LANG_ID`),
  CONSTRAINT `gpt_location_name_ibfk_1` FOREIGN KEY (`LOCATION_ID`) REFERENCES `gpt_location` (`ID`),
  CONSTRAINT `gpt_location_name_ibfk_2` FOREIGN KEY (`LANG_ID`) REFERENCES `gpt_language` (`ID`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `gpt_location_name`
--

LOCK TABLES `gpt_location_name` WRITE;
/*!40000 ALTER TABLE `gpt_location_name` DISABLE KEYS */;
INSERT INTO `gpt_location_name` VALUES (3,1,'Belarus'),(3,2,'Weissrussland'),(3,3,'B?larus'),(3,4,'Беларусь'),(3,5,'Беларусь'),(4,1,'Minsk'),(4,2,'Minsk'),(4,3,'Minsk'),(4,4,'Минск'),(4,5,'М?нск'),(5,1,'Russia'),(5,3,'Russie'),(5,4,'Россия'),(5,5,'Рас?я'),(5,2,'Russland'),(6,1,'Moscow'),(6,2,'Moskau'),(6,3,'Moscou'),(6,4,'Москва'),(6,5,'Масква'),(7,1,'Saint Petersburg'),(7,2,'St. Petersburg'),(7,3,'St. P?tersbourg'),(7,4,'Санкт-Петербург'),(7,5,'Санкт-Пецярбург'),(8,1,'USA'),(8,2,'USA'),(8,3,'USA'),(8,4,'США'),(8,5,'ЗША'),(9,1,'New York'),(9,2,'New York'),(9,3,'New York'),(9,4,'Нью-Йорк'),(9,5,'Нью-Ёрк'),(10,1,'Germany'),(10,2,'Deutschland'),(10,3,'Allemagne'),(10,4,'Германия'),(10,5,'Герман?я'),(11,1,'Berlin'),(11,2,'Berlin'),(11,3,'Berlin'),(11,4,'Берлин'),(11,5,'Берл?н'),(12,1,'Munich'),(12,2,'M?nchen'),(12,3,'Munich'),(12,4,'Мюнхен'),(12,5,'Мюнхен');
/*!40000 ALTER TABLE `gpt_location_name` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `gpt_location_type`
--

DROP TABLE IF EXISTS `gpt_location_type`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `gpt_location_type` (
  `ID` tinyint(3) NOT NULL AUTO_INCREMENT,
  `TYPE_NAME` varchar(32) NOT NULL,
  PRIMARY KEY (`ID`)
) ENGINE=InnoDB AUTO_INCREMENT=4 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `gpt_location_type`
--

LOCK TABLES `gpt_location_type` WRITE;
/*!40000 ALTER TABLE `gpt_location_type` DISABLE KEYS */;
INSERT INTO `gpt_location_type` VALUES (1,'Root'),(2,'Country'),(3,'City');
/*!40000 ALTER TABLE `gpt_location_type` ENABLE KEYS */;
UNLOCK TABLES;
/*!40103 SET TIME_ZONE=@OLD_TIME_ZONE */;

/*!40101 SET SQL_MODE=@OLD_SQL_MODE */;
/*!40014 SET FOREIGN_KEY_CHECKS=@OLD_FOREIGN_KEY_CHECKS */;
/*!40014 SET UNIQUE_CHECKS=@OLD_UNIQUE_CHECKS */;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
/*!40111 SET SQL_NOTES=@OLD_SQL_NOTES */;

-- Dump completed on 2017-10-29 16:50:56
