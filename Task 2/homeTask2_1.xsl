<?xml version="1.0" encoding="UTF-8"?>
<xsl:stylesheet version="1.0"
    xmlns:xsl="http://www.w3.org/1999/XSL/Transform"
    xmlns:h1="HouseChema"
    xmlns:i1="HouseInfo"
    xmlns:r1="RoomsChema">
        
    <xsl:template match = "/">
        <xsl:variable name="smallcase" select="'abcdefghijklmnopqrstuvwxyz'" />
        <xsl:variable name="uppercase" select="'ABCDEFGHIJKLMNOPQRSTUVWXYZ'" />
        
        <AllRooms>
            <xsl:for-each select="//h1:Houses/h1:House">
                <xsl:sort select="@City"/>
                <xsl:variable name="houseRoomsCount" select="count(descendant::r1:Room)"/>
                <xsl:variable name="houseGuestsCount" select="sum(descendant::r1:Room/@guests)"/>
                <xsl:variable name="guestsPerRoomAvg" select="round($houseGuestsCount div $houseRoomsCount)" />
                <xsl:for-each select="h1:Blocks/h1:Block">
                    <xsl:sort select="@number"/>
                    <xsl:for-each select="descendant::r1:Room">
                        <xsl:sort select="@nuber" data-type="number"/>
                        <Room>
                            <Address>
                                <xsl:value-of select="translate(ancestor::h1:House/@City, $smallcase, $uppercase)"/>
                                <xsl:text>/</xsl:text>
                                <xsl:value-of select="ancestor::h1:House/i1:Address"/>
                                <xsl:text>/</xsl:text>
                                <xsl:value-of select="ancestor::h1:Block/@number"/>
                                <xsl:text>/</xsl:text>
                                <xsl:value-of select="self::r1:Room/@nuber"/>
                            </Address>
                            <HouseRoomsCount>
                                <xsl:value-of select="$houseRoomsCount"/>
                            </HouseRoomsCount>
                            <BlockRoomsCount>
                                <xsl:value-of select="count(../r1:Room)"/>
                            </BlockRoomsCount>
                            <HouseGuestsCount>
                                <xsl:value-of select="$houseGuestsCount"/>
                            </HouseGuestsCount>
                            <GuestsPerRoomAverage>
                                <xsl:value-of select="$guestsPerRoomAvg"/>
                            </GuestsPerRoomAverage>
                            <Allocated Single="{@guests = 1}" Double="{@guests = 2}" Triple="{@guests = 3}" Quarter="{@guests = 4}"/>                        
                        </Room>
                        <xsl:text>&#xa;</xsl:text>
                    </xsl:for-each>
                </xsl:for-each>
            </xsl:for-each>
        </AllRooms>
    </xsl:template>
</xsl:stylesheet>