<?xml version="1.0" encoding="UTF-8"?>
<xsl:stylesheet version="1.0"
    xmlns:xsl="http://www.w3.org/1999/XSL/Transform">
    
    <xsl:template match="/">
        <list>
            <xsl:for-each select="/Guests/Guest">
                <xsl:for-each select="./@*">
                    <xsl:value-of select="."/>
                    <xsl:text>|</xsl:text>
                </xsl:for-each>
                
                <xsl:value-of select="Type"/>
                <xsl:text>|</xsl:text>
                <xsl:value-of select="Profile/Address"/>
                <xsl:text>|</xsl:text>
                <xsl:text>#</xsl:text>
            </xsl:for-each>
        </list>
    </xsl:template>
    
</xsl:stylesheet>