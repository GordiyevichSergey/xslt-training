<?xml version="1.0" encoding="UTF-8"?>
<xsl:stylesheet version="1.0"
    xmlns:xsl="http://www.w3.org/1999/XSL/Transform">
    
    <xsl:template match="/">
        <Guests>
            <xsl:variable name="guest1" select="substring-before(., '#')"/>
            <xsl:variable name="temp1" select="substring-after(.,'#')"/>
            <xsl:variable name="guest2" select="substring-before($temp1, '#')"/>
            <xsl:variable name="temp2" select="substring-after($temp1,'#')"/>
            <xsl:variable name="guest3" select="substring-before($temp2, '#')"/>
            <xsl:variable name="temp3" select="substring-after($temp2,'#')"/>
            <xsl:variable name="guest4" select="substring-before($temp3, '#')"/>
            <xsl:variable name="temp4" select="substring-after($temp3,'#')"/>
            <xsl:variable name="guest5" select="substring-before($temp4, '#')"/>
            <xsl:variable name="temp5" select="substring-after($temp4,'#')"/>
            <xsl:variable name="guest6" select="substring-before($temp5, '#')"/>
            <xsl:variable name="temp6" select="substring-after($temp5,'#')"/>
            <xsl:variable name="guest7" select="substring-before($temp6, '#')"/>
            <xsl:variable name="temp7" select="substring-after($temp6,'#')"/>
            <xsl:variable name="guest8" select="substring-before($temp7, '#')"/>
            <xsl:variable name="temp8" select="substring-after($temp7,'#')"/>
            <xsl:variable name="guest9" select="substring-before($temp8, '#')"/>
            
            <xsl:call-template name="createGuestElement">
                <xsl:with-param name="string" select="$guest1"></xsl:with-param>
            </xsl:call-template>
            
            <xsl:call-template name="createGuestElement">
                <xsl:with-param name="string" select="$guest2"></xsl:with-param>
            </xsl:call-template>
            
            <xsl:call-template name="createGuestElement">
                <xsl:with-param name="string" select="$guest3"></xsl:with-param>
            </xsl:call-template>
            
            <xsl:call-template name="createGuestElement">
                <xsl:with-param name="string" select="$guest4"></xsl:with-param>
            </xsl:call-template>
            
            <xsl:call-template name="createGuestElement">
                <xsl:with-param name="string" select="$guest5"></xsl:with-param>
            </xsl:call-template>
            
            <xsl:call-template name="createGuestElement">
                <xsl:with-param name="string" select="$guest6"></xsl:with-param>
            </xsl:call-template>
            
            <xsl:call-template name="createGuestElement">
                <xsl:with-param name="string" select="$guest7"></xsl:with-param>
            </xsl:call-template>
            
            <xsl:call-template name="createGuestElement">
                <xsl:with-param name="string" select="$guest8"></xsl:with-param>
            </xsl:call-template>
            
            <xsl:call-template name="createGuestElement">
                <xsl:with-param name="string" select="$guest9"></xsl:with-param>
            </xsl:call-template>
        </Guests>
    </xsl:template>
    
    <xsl:template name="createGuestElement">
        <xsl:param name="string"/>
        
        <xsl:variable name="age" select="substring-before($string,'|')"/>
        <xsl:variable name="afterAge" select="substring-after($string,'|')"/>
        
        <xsl:variable name="nationality" select="substring-before($afterAge,'|')"/>
        <xsl:variable name="afterNationality" select="substring-after($afterAge,'|')"/>
        
        <xsl:variable name="gender" select="substring-before($afterNationality,'|')"/>
        <xsl:variable name="afterGender" select="substring-after($afterNationality,'|')"/>
        
        <xsl:variable name="name" select="substring-before($afterGender,'|')"/>
        <xsl:variable name="afterName" select="substring-after($afterGender, '|')"/>
        
        <xsl:variable name="type" select="substring-before($afterName,'|')"/>
        <xsl:variable name="afterType" select="substring-after($afterName, '|')"/>
        
        <xsl:variable name="address" select="substring-before($afterType,'|')"/>
        
        <Guest Age='{$age}' Nationality='{$nationality}' Gender='{$gender}' Name='{$name}'>
            <Type><xsl:value-of select="$type"/></Type>
            <Profile>
                <Address><xsl:value-of select="$address"/></Address>
            </Profile>
        </Guest>
    </xsl:template>
</xsl:stylesheet>